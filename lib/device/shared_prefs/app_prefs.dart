import 'package:shared_preferences/shared_preferences.dart';

class AppSharedPreferences {
  static final AppSharedPreferences _instance =
      AppSharedPreferences._internal();

  factory AppSharedPreferences() {
    return _instance;
  }

  AppSharedPreferences._internal();

  static String _valueBalance = "value_balance";

  Future<void> saveCustomerBalance(int newBalance) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(_valueBalance, newBalance);
  }

  Future<int> getCustomerBalance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int? balance = prefs.getInt(_valueBalance);
    if (balance == null) {
      return 0;
    } else {
      return balance;
    }
  }
}
