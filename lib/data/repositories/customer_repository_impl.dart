import 'package:nubank_code_challenge/core/error/success.dart';
import 'package:nubank_code_challenge/data/datasource/data_source.dart';
import 'package:nubank_code_challenge/data/wrappers/customer_wrapper.dart';
import 'package:nubank_code_challenge/device/shared_prefs/app_prefs.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:nubank_code_challenge/domain/repositories/customer_repository.dart';

class CustomerRepositoryImpl implements CustomerRepository {
  DataSource dataSource;
  CustomerRepositoryImpl(this.dataSource);

  @override
  Future<Either<ServerFailure, Customer>> getCustomer() async {
    var response = await dataSource.getCustomerModel();
    return response.fold(
      (failure) => Left(ServerFailure(errorMessage: failure.errorMessage)),
      (customer) => Right(CustomerWrapper.convertCustomer(customer)),
    );
  }

  @override
  Future<Either<ServerFailure, Customer>> buyProduct(String offerId) async {
    var response = await dataSource.buyProduct(offerId);
    return response.fold(
      (failure) => Left(ServerFailure(errorMessage: failure.errorMessage)),
      (customer) => Right(CustomerWrapper.convertCustomer(customer)),
    );
  }

  @override
  Future<Either<Failure, int>> getBalance() async {
    AppSharedPreferences prefs = AppSharedPreferences();
    try {
      int balance = await prefs.getCustomerBalance();
      return Right(balance);
    } on Exception {
      return Left(Failure());
    }
  }

  @override
  Future<Either<Failure, Success>> updateBalance(int newBalance) async {
    AppSharedPreferences prefs = AppSharedPreferences();
    try {
      prefs.saveCustomerBalance(newBalance);
      return Right(Success());
    } on Exception {
      return Left(Failure());
    }
  }
}
