import 'package:graphql/client.dart';

class GraphQueries {
  static QueryOptions getCustomerQuery() {
    return QueryOptions(document: gql(r'''
    query {
      viewer {
        name
        id
        balance
        offers {
          id
          price
          product {
            id
            name
            description
            image
          }
        }
      }
    }
    '''));
  }
}
