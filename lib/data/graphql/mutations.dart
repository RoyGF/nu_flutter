import 'package:graphql/client.dart';

class GraphMutations {
  static MutationOptions getBuyProductMutation(String offerId) {
    return MutationOptions(document: gql(r'''
    mutation MutationRoot($purchaseOfferId: ID!) {
      purchase(offerId: $purchaseOfferId) {
        success
        errorMessage
        customer {
          name
          id
          balance
          offers {
            id
            price
            product {
              id
              name
              description
              image
            }
          }
        }
      }
    }
    '''), variables: <String, dynamic>{
      'purchaseOfferId': offerId,
    });
  }
}
