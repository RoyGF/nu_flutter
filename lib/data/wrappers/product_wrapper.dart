

import 'package:nubank_code_challenge/data/models/product_model.dart';
import 'package:nubank_code_challenge/domain/entities/product.dart';

class ProductWrapper {
  static Product convertProduct(ProductModel productModel) {
    return Product(
      id: productModel.id,
      description: productModel.description,
      name: productModel.name,
      imagePath: productModel.imagePath,
    );
  }
}