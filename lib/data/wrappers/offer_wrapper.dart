import 'package:nubank_code_challenge/data/models/offer_model.dart';
import 'package:nubank_code_challenge/data/wrappers/product_wrapper.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';

class OfferWrapper {
  static Offer convertOffer(OfferModel offerModel) {
    return Offer(
      id: offerModel.id,
      product: ProductWrapper.convertProduct(offerModel.product!),
      price: offerModel.price,
    );
  }
}