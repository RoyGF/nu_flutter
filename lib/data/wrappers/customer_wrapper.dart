import 'package:nubank_code_challenge/data/models/customer_model.dart';
import 'package:nubank_code_challenge/data/models/offer_model.dart';
import 'package:nubank_code_challenge/data/wrappers/offer_wrapper.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';

class CustomerWrapper {
  static Customer convertCustomer(CustomerModel customerModel) {
    return Customer(
      id: customerModel.id,
      name: customerModel.name,
      balance: customerModel.balance,
      offers: convertOffers(customerModel.offers!),
    );
  }

  static List<Offer> convertOffers(List<OfferModel> offerModels) {
    List<Offer> offers = [];

    offerModels.forEach((element) { 
      offers.add(OfferWrapper.convertOffer(element));
    });

    return offers;
  }
}
