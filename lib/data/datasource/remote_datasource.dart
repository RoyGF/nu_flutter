import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:graphql/client.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:nubank_code_challenge/data/datasource/data_source.dart';
import 'package:nubank_code_challenge/data/graphql/mutations.dart';
import 'package:nubank_code_challenge/data/graphql/queries.dart';
import 'package:nubank_code_challenge/data/models/customer_model.dart';
import 'package:nubank_code_challenge/data/models/response_data_model.dart';
import 'package:nubank_code_challenge/device/network/network_info.dart';

class RemoteDataSource implements DataSource {
  RemoteDataSource({@required this.networkInfo, @required this.graphQLClient});

  final NetworkInfo? networkInfo;
  final GraphQLClient? graphQLClient;


  @override
  Future<Either<ServerFailure, CustomerModel>> getCustomerModel() async {
    bool hasConnection = await networkInfo!.isConnected;
    if (!hasConnection) {
      return Left(ServerFailure(errorMessage: "No hay conexión a internet"));
    }

    final GraphQLClient _client = graphQLClient!;
    final QueryOptions options = GraphQueries.getCustomerQuery();
    final QueryResult result = await _client.query(options);

    if (result.hasException) {
      return Left(ServerFailure(errorMessage: result.exception.toString()));
    }
    try {
      ResponseDataModel response = ResponseDataModel.fromJson(result.data!);
      return Right(response.customer!);
    } on Exception {
      return Left(ServerFailure(errorMessage: "Hubo un error en el server"));
    }
  }

  @override
  Future<Either<ServerFailure, CustomerModel>> buyProduct(
      String offerId) async {
    bool hasConnection = await networkInfo!.isConnected;
    if (!hasConnection) {
      return Left(ServerFailure(errorMessage: "No hay conexión a internet"));
    }
    
    if (offerId.isEmpty) {
      return Left(ServerFailure(errorMessage: "Incorrect or empty offer id"));
    }

    final GraphQLClient _client = graphQLClient!;
    final MutationOptions options =
        GraphMutations.getBuyProductMutation(offerId);
    final QueryResult result = await _client.mutate(options);

    if (result.hasException) {
      return Left(ServerFailure(errorMessage: result.exception.toString()));
    }

    try {
      ResponseDataModel response = ResponseDataModel.fromJson(result.data!);
      if (response.purchase == null) {
        return Left(ServerFailure(errorMessage: "No se pudo hacer la compra"));
      }
      if (response.purchase!.success!) {
        return Right(response.purchase!.customer!);
      } else {
        return Left(
            ServerFailure(errorMessage: response.purchase!.errorMessage!));
      }
    } on Exception {
      return Left(ServerFailure(errorMessage: "No se pudo hacer la compra"));
    }
  }
}
