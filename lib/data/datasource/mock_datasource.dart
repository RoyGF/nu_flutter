import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:nubank_code_challenge/data/datasource/data_source.dart';
import 'package:nubank_code_challenge/data/models/customer_model.dart';
import 'package:nubank_code_challenge/data/models/response_model.dart';

class MockDataSource implements DataSource {
  @override
  Future<Either<ServerFailure, CustomerModel>> getCustomerModel() async {
    final String response = await rootBundle.loadString('assets/data.json');
    final data = await json.decode(response);
    ResponseModel resp = ResponseModel.fromJson(data);
    return Right(resp.data!.customer!);
  }

  @override
  Future<Either<ServerFailure, CustomerModel>> buyProduct(String productId) async {

    final String response = await rootBundle.loadString('assets/data.json');
    final data = await json.decode(response);
    ResponseModel resp = ResponseModel.fromJson(data);

    return Right(resp.data!.customer!);
  }
}
