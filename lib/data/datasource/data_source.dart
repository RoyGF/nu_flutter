import 'package:dartz/dartz.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:nubank_code_challenge/data/models/customer_model.dart';

abstract class DataSource {
  Future<Either<ServerFailure, CustomerModel>> getCustomerModel();
  Future<Either<ServerFailure, CustomerModel>> buyProduct(String productId);
}