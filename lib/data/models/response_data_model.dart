import 'package:json_annotation/json_annotation.dart';
import 'package:nubank_code_challenge/data/models/customer_model.dart';
import 'package:nubank_code_challenge/data/models/purchase_model.dart';

part 'response_data_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ResponseDataModel {
  factory ResponseDataModel.fromJson(Map<String, dynamic> json) =>
      _$ResponseDataModelFromJson(json);
  Map<String, dynamic> toJson() => _$ResponseDataModelToJson(this);

  @JsonKey(name: "viewer")
  CustomerModel? customer;
  @JsonKey(name: "purchase")
  PurchaseModel? purchase;

  ResponseDataModel({
    this.customer,
    this.purchase,
  });
}
