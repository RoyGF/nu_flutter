import 'package:json_annotation/json_annotation.dart';
import 'package:nubank_code_challenge/data/models/response_data_model.dart';
part 'response_model.g.dart';

@JsonSerializable(explicitToJson: true)
class ResponseModel {
  factory ResponseModel.fromJson(Map<String, dynamic> json) => _$ResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$ResponseModelToJson(this);

  @JsonKey(name: "data")
  ResponseDataModel? data;

  ResponseModel({
    this.data,
  });

}