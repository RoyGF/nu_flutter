// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseDataModel _$ResponseDataModelFromJson(Map<String, dynamic> json) {
  return ResponseDataModel(
    customer: json['viewer'] == null
        ? null
        : CustomerModel.fromJson(json['viewer'] as Map<String, dynamic>),
    purchase: json['purchase'] == null
        ? null
        : PurchaseModel.fromJson(json['purchase'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ResponseDataModelToJson(ResponseDataModel instance) =>
    <String, dynamic>{
      'viewer': instance.customer?.toJson(),
      'purchase': instance.purchase?.toJson(),
    };
