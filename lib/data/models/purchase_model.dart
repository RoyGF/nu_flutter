

import 'package:json_annotation/json_annotation.dart';
import 'package:nubank_code_challenge/data/models/customer_model.dart';
part 'purchase_model.g.dart';

@JsonSerializable(explicitToJson: true)
class PurchaseModel {
  factory PurchaseModel.fromJson(Map<String, dynamic> json) => _$PurchaseModelFromJson(json);
  Map<String, dynamic> toJson() => _$PurchaseModelToJson(this);

  @JsonKey(name: "success")
  bool? success;
  @JsonKey(name: "errorMessage")
  String? errorMessage;
  @JsonKey(name: "customer") 
  CustomerModel? customer;

  PurchaseModel({
    this.success, 
    this.errorMessage,
    this.customer
  });

}