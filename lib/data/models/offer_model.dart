import 'package:json_annotation/json_annotation.dart';
import 'package:nubank_code_challenge/data/models/product_model.dart';
part 'offer_model.g.dart';

@JsonSerializable(explicitToJson: true)
class OfferModel {
  factory OfferModel.fromJson(Map<String, dynamic> json) => _$OfferModelFromJson(json);
  Map<String, dynamic> toJson() => _$OfferModelToJson(this);

  @JsonKey(name: "id")
  String? id;
  @JsonKey(name: "price")
  int? price;
  @JsonKey(name: "product")
  ProductModel? product;

  OfferModel({
    this.id,
    this.price,
    this.product,
  });

}