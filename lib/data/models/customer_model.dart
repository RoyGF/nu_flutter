import 'package:json_annotation/json_annotation.dart';
import 'package:nubank_code_challenge/data/models/offer_model.dart';
part 'customer_model.g.dart';

@JsonSerializable(explicitToJson: true)
class CustomerModel {
  factory CustomerModel.fromJson(Map<String, dynamic> json) => _$CustomerModelFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerModelToJson(this);

  @JsonKey(name: "id")
  String? id;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "balance")
  int? balance;
  @JsonKey(name: "offers")
  List<OfferModel>? offers;

  CustomerModel({
    this.id,
    this.name,
    this.balance,
    this.offers,
  });

}