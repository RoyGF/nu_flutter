// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'purchase_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PurchaseModel _$PurchaseModelFromJson(Map<String, dynamic> json) {
  return PurchaseModel(
    success: json['success'] as bool?,
    errorMessage: json['errorMessage'] as String?,
    customer: json['customer'] == null
        ? null
        : CustomerModel.fromJson(json['customer'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PurchaseModelToJson(PurchaseModel instance) =>
    <String, dynamic>{
      'success': instance.success,
      'errorMessage': instance.errorMessage,
      'customer': instance.customer?.toJson(),
    };
