import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:nubank_code_challenge/data/datasource/data_source.dart';
import 'package:nubank_code_challenge/data/datasource/remote_datasource.dart';
import 'package:nubank_code_challenge/data/graphql/client.dart';
import 'package:nubank_code_challenge/data/repositories/customer_repository_impl.dart';
import 'package:nubank_code_challenge/device/network/network_info.dart';
import 'package:nubank_code_challenge/domain/repositories/customer_repository.dart';
import 'package:nubank_code_challenge/domain/usecases/buy_product.dart';
import 'package:nubank_code_challenge/domain/usecases/get_customer.dart';
import 'package:nubank_code_challenge/domain/usecases/get_customer_balance.dart';
import 'package:nubank_code_challenge/domain/usecases/update_customer_balance.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_bloc.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_states.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_bloc.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_states.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // Blocs
  sl.registerFactory(
    () => UserInfoBloc(
      UserInfoStateInitial(),
      getCustomer: sl(),
      updateCustomerBalance: sl(),
    ),
  );

  sl.registerFactory(
    () => ProductDetailBloc(
      ProductDetailStateInitial(),
      buyProduct: sl(),
      updateCustomerBalance: sl(),
      getCustomerBalance: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => GetCustomer(sl()));
  sl.registerLazySingleton(() => BuyProduct(sl()));
  sl.registerLazySingleton(() => UpdateCustomerBalance(sl()));
  sl.registerLazySingleton(() => GetCustomerBalance(sl()));

  // Repositories
  sl.registerLazySingleton<CustomerRepository>(
      () => CustomerRepositoryImpl(sl()));

  // Data sources
  RemoteClient remoteClient = RemoteClient();
  sl.registerLazySingleton<DataSource>(
    () => RemoteDataSource(
      networkInfo: sl(),
      graphQLClient: remoteClient.getGraphQLClient(),
    ),
  );

  // network connection
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
