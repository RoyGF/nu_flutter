import 'package:flutter/material.dart';
import 'package:nubank_code_challenge/presentation/user_info/pages/user_info_page.dart';

class PurchaseSuccessPage extends StatelessWidget {
  const PurchaseSuccessPage({
    Key? key,
    @required this.newBalance,
  }) : super(key: key);

  final int? newBalance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nu Bank'),
      ),
      body: Column(
        children: [
          Text('Compra exitosa'),
          Text("Nuevo Balance: $newBalance"),
          Spacer(),
          Container(
            color: Colors.purple,
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: TextButton(
              onPressed: () => _close(context, this.newBalance!),
              child: Text(
                'Cerrar',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _close(BuildContext context, int inNewBalance) {
    Route route = MaterialPageRoute(
      builder: (_) => UserInfoPage(newBalance: inNewBalance),
    );
    Navigator.pushReplacement(context, route);
  }
}
