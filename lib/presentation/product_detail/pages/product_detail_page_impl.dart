import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_bloc.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_states.dart';
import 'package:nubank_code_challenge/presentation/product_detail/widgets/product_detail_body.dart';
import 'package:nubank_code_challenge/presentation/product_detail/widgets/product_detail_error.dart';
import 'package:nubank_code_challenge/presentation/purchase_success/pages/purchase_success_page.dart';

class ProductDetailPageImpl extends StatefulWidget {
  ProductDetailPageImpl();

  @override
  _ProductDetailPageImplState createState() => _ProductDetailPageImplState();
}

class _ProductDetailPageImplState extends State<ProductDetailPageImpl> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<ProductDetailBloc, ProductDetailState>(
      listener: _blocListener,
      child: BlocBuilder<ProductDetailBloc, ProductDetailState>(
        builder: _blocBuilder,
      ),
    );
  }

  void _blocListener(BuildContext context, ProductDetailState state) {
    if (state is PoductDetailStateBuySuccess) {
      _openSuccessBuyPage(context, state.newBalance);
    }
  }

  Widget _blocBuilder(BuildContext context, ProductDetailState state) {
    if (state is ProductDetailStateShow) {
      return ProductDetailBody(offer: state.offer);
    }
    if (state is ProductDetailError) {
      return ProductDetailErrorPage(errorMessage: state.errorMessage);
    }
    if (state is PoductDetailStateBuySuccess) {
      return Container();
    }
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void _openSuccessBuyPage(BuildContext context, int inNewBalance) {
    Route route = MaterialPageRoute(
      builder: (_) => PurchaseSuccessPage(newBalance: inNewBalance),
    );
    Navigator.pushAndRemoveUntil(context, route, (route) => false);
  }
}
