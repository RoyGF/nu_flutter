import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nubank_code_challenge/dependency_injection/injection_container.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_bloc.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_events.dart';
import 'package:nubank_code_challenge/presentation/product_detail/pages/product_detail_page_impl.dart';


class ProductDetailPage extends StatelessWidget {

  const ProductDetailPage({Key? key, @required this.offer}) : super(key: key);

  final Offer? offer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  BlocProvider<ProductDetailBloc> _buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<ProductDetailBloc>()..add(ProductDetailEventInitial(offer!)),
      child: ProductDetailPageImpl(),
    );
  }
}