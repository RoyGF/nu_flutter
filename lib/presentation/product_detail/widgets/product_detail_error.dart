import 'package:flutter/material.dart';

class ProductDetailErrorPage extends StatelessWidget {
  const ProductDetailErrorPage({
    Key? key,
    @required this.errorMessage,
  }) : super(key: key);

  final String? errorMessage;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Nu Bank'),
        ),
        body: Column(
          children: [
            Expanded(
              child: Center(
                child: Text(errorMessage!),
              ),
            ),
            Container(
              color: Colors.purple,
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: TextButton(
                onPressed: () => _return(context),
                child: Text(
                  'Regresar',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _return(BuildContext context) {
    Navigator.pop(context);
  }
}
