import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_bloc.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_events.dart';

class ProductDetailBody extends StatelessWidget {
  const ProductDetailBody({Key? key, @required this.offer}) : super(key: key);

  final Offer? offer;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Nu Bank'),
        ),
        body: Container(
          child: Column(
            children: [
              Image(
                width: 200,
                height: 200,
                image: NetworkImage(offer!.product!.imagePath!),
              ),
              Text('${offer!.product!.description}'),
              Row(
                children: [
                  Text(
                    'Precio: ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  Text("\$${offer!.price!}"),
                ],
              ),
              Spacer(),
              Container(
                color: Colors.purple,
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: TextButton(
                  onPressed: () => _makePurchase(context, offer!),
                  child: Text(
                    'Comprar',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _makePurchase(BuildContext context, Offer inOffer) {
    BlocProvider.of<ProductDetailBloc>(context)
        .add(ProductDetailEventBuy(inOffer));
  }
}
