import 'package:equatable/equatable.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';

abstract class ProductDetailState extends Equatable {
  const ProductDetailState();

  @override
  List<Object> get props => [];
}

class ProductDetailStateInitial extends ProductDetailState {}

class ProductDetailStateShow extends ProductDetailState {
  const ProductDetailStateShow(this.offer);

  final Offer offer;

  @override
  List<Object> get props => [offer];
}

class ProductDetailStateLoading extends ProductDetailState {}

class ProductDetailError extends ProductDetailState {
  const ProductDetailError(this.errorMessage);

  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}

class PoductDetailStateBuySuccess extends ProductDetailState {
  const PoductDetailStateBuySuccess(this.newBalance);

  final int newBalance;

  @override
  List<Object> get props => [newBalance];
}
