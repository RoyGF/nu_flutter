import 'package:equatable/equatable.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';

abstract class ProductDetailEvent extends Equatable {
  const ProductDetailEvent();

  @override
  List<Object> get props => [];
}

class ProductDetailEventInitial extends ProductDetailEvent {
  const ProductDetailEventInitial(this.offer);

  final Offer offer;

  @override
  List<Object> get props => [offer];
}

class ProductDetailEventBuy extends ProductDetailEvent {
  
  const ProductDetailEventBuy(this.offer);
  
  final Offer offer;
  @override
  List<Object> get props => [offer];
}
