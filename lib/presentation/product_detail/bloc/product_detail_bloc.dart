import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/usecases/buy_product.dart';
import 'package:nubank_code_challenge/domain/usecases/get_customer_balance.dart';
import 'package:nubank_code_challenge/domain/usecases/update_customer_balance.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_events.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_states.dart';

class ProductDetailBloc extends Bloc<ProductDetailEvent, ProductDetailState> {
  ProductDetailBloc(
    ProductDetailState initialState, {
    @required this.buyProduct,
    @required this.updateCustomerBalance,
    @required this.getCustomerBalance,
  })  : assert(buyProduct != null),
        assert(updateCustomerBalance != null),
        assert(getCustomerBalance != null),
        super(initialState);

  final BuyProduct? buyProduct;
  final UpdateCustomerBalance? updateCustomerBalance;
  final GetCustomerBalance? getCustomerBalance;

  @override
  Stream<ProductDetailState> mapEventToState(ProductDetailEvent event) async* {
    if (event is ProductDetailEventInitial) {
      yield ProductDetailStateShow(event.offer);
    }

    if (event is ProductDetailEventBuy) {
      yield ProductDetailStateLoading();
      var balanceResponse = await getCustomerBalance!.call(NoParams());
      if (balanceResponse.isRight()) {
        int balance = balanceResponse.getOrElse(() => 0);

        if (balance < event.offer.price!) {
          yield ProductDetailError(
              "No hay suficientes fondos para hacer la compra");
        } else {
          int newBalance = balance - event.offer.price!;
          var buyingResponse =
              await buyProduct!.call(BuyProductParams(event.offer.id!));
          if (buyingResponse.isRight()) {
            await updateCustomerBalance!.call(
                UpdateCustomerBalanceParams(newBalance));
          }
          yield* _foldBuyingResponse(buyingResponse, newBalance);
        }
      } else {
        yield ProductDetailError("Hubo un error al procesar la compra");
      }
    }
  }

  Stream<ProductDetailState> _foldBuyingResponse(
      Either<ServerFailure, Customer> buyingResponse, int inNewBalance) async* {
    yield buyingResponse.fold(
      (failure) => ProductDetailError(failure.errorMessage!),
      (customer) => PoductDetailStateBuySuccess(inNewBalance),
    );
  }
}
