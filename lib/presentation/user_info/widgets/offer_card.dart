import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_bloc.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_events.dart';

class OfferCard extends StatelessWidget {
  const OfferCard({Key? key, @required this.offer}) : super(key: key);
  final Offer? offer;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () => _openOfferDetail(context, offer!),
      child: Container(
        margin: EdgeInsets.all(15),
        child: Column(
          children: [
            Text('${offer!.product!.name}',
                style: TextStyle(color: Colors.black)),
            Image(
              width: 150,
              height: 150,
              image: NetworkImage(offer!.product!.imagePath!),
            ),
          ],
        ),
      ),
    );
  }

  void _openOfferDetail(BuildContext context, Offer inOffer) {
    // This object is made so the equatable comparison always returns a
    // different object, otherwise, it will not emit the map event to state
    // method inside the BLoC class.
    Offer newOffer = Offer(
      id: inOffer.id,
      price: inOffer.price,
      product: inOffer.product,
    );
    BlocProvider.of<UserInfoBloc>(context)
        .add(UserInfoEventOpenOffer(newOffer));
  }
}
