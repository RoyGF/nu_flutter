import 'package:equatable/equatable.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';
import 'package:meta/meta.dart';

@immutable
abstract class UserInfoState extends Equatable {
  const UserInfoState();
  
  @override
  List<Object> get props => [];
}

class UserInfoStateInitial extends UserInfoState {}

class UserInfoStateLoading extends UserInfoState {}

class UserInfoStateLoaded extends UserInfoState {
  const UserInfoStateLoaded(this.customer);

  final Customer customer;

  @override
  List<Object> get props => [customer];
}

class UserInfoStateError extends UserInfoState {
  const UserInfoStateError(this.errorMessage);

  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}

class UserInfoStateOpenOffer extends UserInfoState {
  final Offer offer;

  const UserInfoStateOpenOffer(this.offer);

  @override
  List<Object> get props => [offer];
}
