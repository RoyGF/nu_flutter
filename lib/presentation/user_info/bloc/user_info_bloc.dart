import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/usecases/get_customer.dart';
import 'package:nubank_code_challenge/domain/usecases/update_customer_balance.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_events.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_states.dart';

class UserInfoBloc extends Bloc<UserInfoEvent, UserInfoState> {
  UserInfoBloc(
    UserInfoState initialState, {
    @required this.getCustomer,
    @required this.updateCustomerBalance,
  }) : super(initialState);

  final GetCustomer? getCustomer;
  final UpdateCustomerBalance? updateCustomerBalance;

  @override
  Stream<UserInfoState> mapEventToState(UserInfoEvent event) async* {
    if (event is UserInfoEventInitial) {
      var response = await getCustomer?.call(NoParams());
      if (response != null && response.isRight()) {
        Customer customer =
            response.getOrElse(() => Customer(name: "", balance: 0, id: "", offers: []));
        if (event.newBalance != null) {
          customer.balance = event.newBalance;
        }
        await updateCustomerBalance!.call(UpdateCustomerBalanceParams(customer.balance!));
        yield (UserInfoStateLoaded(customer));
      } else {
        yield UserInfoStateError("errorMessage");
      }
    }

    if (event is UserInfoEventOpenOffer) {
      yield UserInfoStateOpenOffer(event.offer);
    }
  }
}
