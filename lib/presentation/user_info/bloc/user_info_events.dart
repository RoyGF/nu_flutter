import 'package:meta/meta.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';

abstract class UserInfoEvent {
  const UserInfoEvent();

  List<Object> get props => [];
}

class UserInfoEventInitial extends UserInfoEvent {
  const UserInfoEventInitial({@required this.newBalance});
  final int? newBalance;

  @override
  List<Object> get props => [newBalance == null ? 0 : newBalance!];
}

class UserInfoEventOpenOffer extends UserInfoEvent {
  const UserInfoEventOpenOffer(this.offer);

  final Offer offer;

  @override
  List<Object> get props => [offer];
}
