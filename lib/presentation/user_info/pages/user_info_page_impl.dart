import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';
import 'package:nubank_code_challenge/presentation/product_detail/pages/product_detail_page.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_bloc.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_states.dart';
import 'package:nubank_code_challenge/presentation/user_info/widgets/offer_card.dart';

class UserInfoPageImpl extends StatefulWidget {
  UserInfoPageImpl();

  @override
  _UserInfoPageImplState createState() => _UserInfoPageImplState();
}

class _UserInfoPageImplState extends State<UserInfoPageImpl> {
  Customer? customer;

  @override
  Widget build(BuildContext context) {
    return BlocListener<UserInfoBloc, UserInfoState>(
      listener: _blocListener,
      child: BlocBuilder<UserInfoBloc, UserInfoState>(
        builder: _blocBuilder,
      ),
    );
  }

  void _blocListener(BuildContext context, UserInfoState state) {
    if (state is UserInfoStateLoaded) {
      this.customer = state.customer;
    }

    if (state is UserInfoStateOpenOffer) {
      _openOfferDetailPage(context, state.offer);
    }
    setState(() {});
  }

  Widget _blocBuilder(BuildContext context, UserInfoState state) {
    return Stack(
      children: [
        _body(context, customer),
        Center(
          child: Container(
            child: state is UserInfoStateLoading
                ? CircularProgressIndicator()
                : null,
          ),
        ),
      ],
    );
  }

  Widget _body(BuildContext context, Customer? customer) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Nu Bank'),
        ),
        body: customer == null
            ? Container()
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Nombre: ${customer.name}'),
                  Text('Balance: ${customer.balance}'),
                  Expanded(
                    child: ListView.builder(
                      itemCount: customer.offers!.length,
                      itemBuilder: (context, index) {
                        return OfferCard(offer: customer.offers![index]);
                      },
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  void _openOfferDetailPage(BuildContext context, Offer inOffer) {
    Route route =
        MaterialPageRoute(builder: (_) => ProductDetailPage(offer: inOffer));
    Navigator.push(context, route);
  }
}
