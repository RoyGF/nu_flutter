import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nubank_code_challenge/dependency_injection/injection_container.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_bloc.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_events.dart';
import 'package:nubank_code_challenge/presentation/user_info/pages/user_info_page_impl.dart';

class UserInfoPage extends StatelessWidget {
  const UserInfoPage({Key? key, this.newBalance}) : super(key: key);

  final int? newBalance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  BlocProvider<UserInfoBloc> _buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<UserInfoBloc>()..add(UserInfoEventInitial(newBalance: newBalance)),
      child: UserInfoPageImpl(),
    );
  }
}
