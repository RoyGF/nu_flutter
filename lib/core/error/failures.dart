import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Failure extends Equatable {
  @override
  List<Object> get props => [];
}

class ServerFailure extends Failure {
  ServerFailure({@required this.errorMessage});
  final String? errorMessage;
}
