import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/repositories/customer_repository.dart';

class GetCustomer implements BaseUseCase<Customer, NoParams> {
  final CustomerRepository repository;

  GetCustomer(this.repository);

  @override
  Future<Either<Failure, Customer>> call(NoParams params) async {
    return await repository.getCustomer();
  }
}
