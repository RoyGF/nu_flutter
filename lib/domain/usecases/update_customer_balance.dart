import 'package:equatable/equatable.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:nubank_code_challenge/core/error/success.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/repositories/customer_repository.dart';

class UpdateCustomerBalance implements BaseUseCase<Success, UpdateCustomerBalanceParams> {
  UpdateCustomerBalance(this.customerRepository);

  CustomerRepository customerRepository;

  @override
  Future<Either<Failure, Success>> call(params) async {
    return await customerRepository.updateBalance(params.newBalance);
  }
}

class UpdateCustomerBalanceParams extends Equatable {
  UpdateCustomerBalanceParams(this.newBalance);

  final int newBalance;

  @override
  List<Object?> get props => [newBalance];
}
