import 'package:equatable/equatable.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/repositories/customer_repository.dart';

class BuyProduct implements BaseUseCase<Customer, BuyProductParams> {
  BuyProduct(this.repository);

  final CustomerRepository repository;

  @override
  Future<Either<ServerFailure, Customer>> call(BuyProductParams params) async {
    return await repository.buyProduct(params.productId);
  }
}

class BuyProductParams extends Equatable {
  final String productId;

  BuyProductParams(this.productId);

  @override
  List<Object> get props => [productId];
}
