import 'package:dartz/dartz.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/repositories/customer_repository.dart';

class GetCustomerBalance implements BaseUseCase<int, NoParams> {
  GetCustomerBalance(this.repository);

  final CustomerRepository repository;

  @override
  Future<Either<Failure, int>> call(NoParams params) async {
    return await repository.getBalance();
  }
}
