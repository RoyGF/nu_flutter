import 'package:meta/meta.dart';

class Product {
  String? id;
  String? name;
  String? description;
  String? imagePath;

  Product({
    @required this.id,
    @required this.name,
    @required this.description,
    @required this.imagePath,
  });
}
