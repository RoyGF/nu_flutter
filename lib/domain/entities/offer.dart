import 'package:nubank_code_challenge/domain/entities/product.dart';
import 'package:meta/meta.dart';

class Offer  {
  String? id;
  int? price;
  Product? product;

  Offer({
    @required this.id,
    @required this.price,
    @required this.product,
  });

}
