import 'package:dartz/dartz.dart';
import 'package:nubank_code_challenge/core/error/failures.dart';
import 'package:nubank_code_challenge/core/error/success.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';

abstract class CustomerRepository {
  Future<Either<Failure, Customer>> getCustomer();
  Future<Either<ServerFailure, Customer>> buyProduct(String productId);
  Future<Either<Failure, int>> getBalance();
  Future<Either<Failure, Success>> updateBalance(int newBalance);
}
