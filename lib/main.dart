import 'package:flutter/material.dart';
import 'package:nubank_code_challenge/presentation/user_info/pages/user_info_page.dart';
import 'dependency_injection/injection_container.dart' as di;
 
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Nubank Coding Challenge',
      home: UserInfoPage(),
      theme: ThemeData(
        primaryColor: Colors.purple,
      )
    );
  }
}