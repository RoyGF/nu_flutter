import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:nubank_code_challenge/core/error/success.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';
import 'package:nubank_code_challenge/domain/usecases/buy_product.dart';
import 'package:nubank_code_challenge/domain/usecases/get_customer_balance.dart';
import 'package:nubank_code_challenge/domain/usecases/update_customer_balance.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_bloc.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_events.dart';
import 'package:nubank_code_challenge/presentation/product_detail/bloc/product_detail_states.dart';

import 'product_detail_bloc_tests.mocks.dart';

@GenerateMocks(
    [BuyProduct, UpdateCustomerBalance, GetCustomerBalance, Offer, Customer])
void main() {
  final mockBuyProduct = MockBuyProduct();
  final mockUpdateCustomerBalance = MockUpdateCustomerBalance();
  final mockGetCustomerBalance = MockGetCustomerBalance();
  final mockOffer = MockOffer();
  final mockCustomer = MockCustomer();

  void setUpOfferPrice(int price) {
    when(mockOffer.price).thenReturn(price);
  }

  blocTest(
    'Product Detail Event Initial',
    build: () => ProductDetailBloc(
      ProductDetailStateInitial(),
      buyProduct: mockBuyProduct,
      updateCustomerBalance: mockUpdateCustomerBalance,
      getCustomerBalance: mockGetCustomerBalance,
    ),
    act: (ProductDetailBloc bloc) =>
        bloc.add(ProductDetailEventInitial(mockOffer)),
    expect: () => [ProductDetailStateShow(mockOffer)],
  );

  group(
    'Product Detail Event Buy',
    () {
      when(mockGetCustomerBalance.call(any))
          .thenAnswer((realInvocation) async => Right(200));
      when(mockBuyProduct.call(any))
          .thenAnswer((realInvocation) async => Right(mockCustomer));
      when(mockUpdateCustomerBalance.call(any))
          .thenAnswer((realInvocation) async => Right(Success()));
      when(mockOffer.id).thenAnswer((realInvocation) => "...");
      blocTest(
        'buy product success',
        build: () => ProductDetailBloc(
          ProductDetailStateInitial(),
          buyProduct: mockBuyProduct,
          updateCustomerBalance: mockUpdateCustomerBalance,
          getCustomerBalance: mockGetCustomerBalance,
        ),
        act: (ProductDetailBloc bloc) {
          setUpOfferPrice(190);
          bloc.add(ProductDetailEventBuy(mockOffer));
        },
        expect: () =>
            [ProductDetailStateLoading(), PoductDetailStateBuySuccess(10)],
      );
      blocTest(
        'buy product failed',
        build: () => ProductDetailBloc(
          ProductDetailStateInitial(),
          buyProduct: mockBuyProduct,
          updateCustomerBalance: mockUpdateCustomerBalance,
          getCustomerBalance: mockGetCustomerBalance,
        ),
        act: (ProductDetailBloc bloc) {
          setUpOfferPrice(210);
          bloc.add(ProductDetailEventBuy(mockOffer));
        },
        expect: () => [
          ProductDetailStateLoading(),
          ProductDetailError("No hay suficientes fondos para hacer la compra")
        ],
      );
    },
  );
}
