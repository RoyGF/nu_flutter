import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:nubank_code_challenge/core/error/success.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/entities/offer.dart';
import 'package:nubank_code_challenge/domain/usecases/get_customer.dart';
import 'package:nubank_code_challenge/domain/usecases/update_customer_balance.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_bloc.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_events.dart';
import 'package:nubank_code_challenge/presentation/user_info/bloc/user_info_states.dart';

import 'user_info_bloc_test.mocks.dart';

@GenerateMocks([GetCustomer, UpdateCustomerBalance, Offer])
void main() {
  final mockUserInfoState = UserInfoStateInitial();
  final mockGetCustomer = MockGetCustomer();
  final mockUpdateCustomerBalance = MockUpdateCustomerBalance();
  final mockOffer = MockOffer();

  final Customer tCustomer = Customer(
    id: "",
    balance: 200,
    name: "",
    offers: [],
  );

  group(
    'UserInfoEventInitial',
    () {
      when(mockGetCustomer.call(NoParams()))
          .thenAnswer((realInvocation) async => Right(tCustomer));
      when(mockUpdateCustomerBalance.call(UpdateCustomerBalanceParams(200)))
          .thenAnswer((realInvocation) async => Right(Success()));

      blocTest(
        'user info bloc with balance 200',
        build: () => UserInfoBloc(
          mockUserInfoState,
          getCustomer: mockGetCustomer,
          updateCustomerBalance: mockUpdateCustomerBalance,
        ),
        act: (UserInfoBloc bloc) =>
            bloc.add(UserInfoEventInitial(newBalance: 200)),
        expect: () => [UserInfoStateLoaded(tCustomer)],
      );

      blocTest(
        'user info bloc with balance null',
        build: () => UserInfoBloc(
          mockUserInfoState,
          getCustomer: mockGetCustomer,
          updateCustomerBalance: mockUpdateCustomerBalance,
        ),
        act: (UserInfoBloc bloc) =>
            bloc.add(UserInfoEventInitial(newBalance: null)),
        expect: () => [UserInfoStateLoaded(tCustomer)],
      );
    },
  );

  group(
    'open offer group',
    () {
      blocTest(
        'User Info Event Open Offer',
        build: () => UserInfoBloc(
          mockUserInfoState,
          getCustomer: mockGetCustomer,
          updateCustomerBalance: mockUpdateCustomerBalance,
        ),
        act: (UserInfoBloc bloc) => bloc.add(UserInfoEventOpenOffer(mockOffer)),
        expect: () => [UserInfoStateOpenOffer(mockOffer)],
      );
    },
  );
}
