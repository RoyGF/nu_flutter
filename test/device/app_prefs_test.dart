import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:nubank_code_challenge/device/shared_prefs/app_prefs.dart';

import 'app_prefs_test.mocks.dart';

@GenerateMocks([AppSharedPreferences])
void main() {
  var mockSharedPreferences = MockAppSharedPreferences();

  group('Tests Balance Save', () {
    test('Check Balance save', () async {
      int balance = 20;
      final Future<int> testBalance = Future.value(balance);

      mockSharedPreferences.saveCustomerBalance(balance);
      verify(mockSharedPreferences.saveCustomerBalance(balance)).called(1);

      when(mockSharedPreferences.getCustomerBalance())
          .thenAnswer((realInvocation) => testBalance);

      final result = mockSharedPreferences.getCustomerBalance();
      expect(result, testBalance);
    });
  });
}
