import 'package:flutter_test/flutter_test.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:nubank_code_challenge/device/network/network_info.dart';

import 'network_test.mocks.dart';

@GenerateMocks([
  InternetConnectionChecker
])
void main() {
  var mockDataConnectionChecker = MockInternetConnectionChecker();
  var mockNetworkInfo = NetworkInfoImpl(mockDataConnectionChecker);

  group('isConnected', () {
    test('should forward the call to InternetConnectionChecker.hasConnection',
        () async {
      final Future<bool> testHasConnectionFuture = Future.value(true);
      when(mockDataConnectionChecker.hasConnection)
          .thenAnswer((_) => testHasConnectionFuture);

      final result = mockNetworkInfo.isConnected;

      verify(mockDataConnectionChecker.hasConnection);
      expect(result, testHasConnectionFuture);
    });
  });
}
