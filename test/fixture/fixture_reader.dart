import 'dart:convert';
import 'dart:io';
// import 'package:flutter/services.dart';

Future<Map<String, dynamic>> fixture(String fileName) async {
  final String response = File('test/fixture/$fileName').readAsStringSync();
  final data = await json.decode(response);
  return data;
}
