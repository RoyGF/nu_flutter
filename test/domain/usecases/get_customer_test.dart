import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/usecases/get_customer.dart';

import 'buy_product_test.mocks.dart';

main() {
  var mockCustomerRepository = MockCustomerRepository();
  var getCustomer = GetCustomer(mockCustomerRepository);

  final tCustomer = Customer(
    id: "",
    offers: [],
    balance: 200,
    name: "test customer",
  );

  test('should return Customer', () async {
    when(mockCustomerRepository.getCustomer())
        .thenAnswer((realInvocation) async => Right(tCustomer));
    final result = await getCustomer.call(NoParams());
    expect(result, Right(tCustomer));
    verify(mockCustomerRepository.getCustomer()).called(1);
    verifyNoMoreInteractions(mockCustomerRepository);
  });
}
