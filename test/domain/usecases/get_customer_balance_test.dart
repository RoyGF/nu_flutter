import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nubank_code_challenge/core/usecases/base_use_case.dart';
import 'package:nubank_code_challenge/domain/usecases/get_customer_balance.dart';

import 'buy_product_test.mocks.dart';

main() {
  var mockCustomerRepository = MockCustomerRepository();

  GetCustomerBalance customerBalance =
      GetCustomerBalance(mockCustomerRepository);

  test('should return balance', () async {
    when(mockCustomerRepository.getBalance())
        .thenAnswer((realInvocation) async => Right(200));

    final result = await customerBalance.call(NoParams());
    expect(result, Right(200));
    verify(mockCustomerRepository.getBalance()).called(1);
    verifyNoMoreInteractions(mockCustomerRepository);
  });
}
