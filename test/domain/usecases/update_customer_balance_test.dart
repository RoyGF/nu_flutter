import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nubank_code_challenge/core/error/success.dart';
import 'package:nubank_code_challenge/domain/usecases/update_customer_balance.dart';

import 'buy_product_test.mocks.dart';

main() {
  var mockCustomerRepository = MockCustomerRepository();

  UpdateCustomerBalance updateCustomerBalance =
      UpdateCustomerBalance(mockCustomerRepository);

  final tParams = UpdateCustomerBalanceParams(200);

  test('should update customer balance', () async {
    when(mockCustomerRepository.updateBalance(any))
        .thenAnswer((realInvocation) async => Right(Success()));

    final response = await updateCustomerBalance.call(tParams);
    expect(response, Right(Success()));
    verify(mockCustomerRepository.updateBalance(any)).called(1);
    verifyNoMoreInteractions(mockCustomerRepository);
  });
}
