import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:nubank_code_challenge/domain/entities/customer.dart';
import 'package:nubank_code_challenge/domain/repositories/customer_repository.dart';
import 'package:nubank_code_challenge/domain/usecases/buy_product.dart';

import 'buy_product_test.mocks.dart';

@GenerateMocks([CustomerRepository])
main() {
  var mockCustomerRepository = MockCustomerRepository();
  var buyProduct = BuyProduct(mockCustomerRepository);

  final tCustomer = Customer(
    id: "",
    offers: [],
    balance: 200,
    name: "test customer",
  );

  final tParams = BuyProductParams("productId");
  test('should buy product', () async {
    when(mockCustomerRepository.buyProduct(any))
        .thenAnswer((realInvocation) async => Right(tCustomer));
    final result = await buyProduct.call(tParams);
    expect(result, Right(tCustomer));
    verify(mockCustomerRepository.buyProduct(any)).called(1);
    verifyNoMoreInteractions(mockCustomerRepository);
  });
}
