import 'package:flutter_test/flutter_test.dart';
import 'package:graphql/client.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:nubank_code_challenge/data/datasource/remote_datasource.dart';
import 'package:nubank_code_challenge/device/network/network_info.dart';

import '../../fixture/fixture_reader.dart';
import 'remote_datasource_test.mocks.dart';

@GenerateMocks([NetworkInfoImpl, GraphQLClient])
void main() {
  var mockNetworkInfoImpl = MockNetworkInfoImpl();
  var mockGraphQLClient = MockGraphQLClient();

  var remoteDataSource = RemoteDataSource(
    networkInfo: mockNetworkInfoImpl,
    graphQLClient: mockGraphQLClient,
  );

  Future<bool> networkResponse = Future.value(true);
  when(mockNetworkInfoImpl.isConnected)
      .thenAnswer((realInvocation) => networkResponse);

  Future<QueryResult> getQueryResult(String fileName) async {
    var fileResponse = await fixture(fileName);
    return QueryResult(data: fileResponse, source: QueryResultSource.network);
  }

  Future<void> setUpSuccessCustomerResponse() async {
    when(mockGraphQLClient.query(any)).thenAnswer(
        (realInvocation) => getQueryResult('success_response.json'));
  }

  Future<void> setUpFailedCustomerResponse() async {
    QueryResult result = await getQueryResult('unauthorized.json');
    result.exception = OperationException(graphqlErrors: []);
    when(mockGraphQLClient.query(any))
        .thenAnswer((realInvocation) => Future.value(result));
  }

  Future<void> setUpSuccessBuyResponse() async {
    when(mockGraphQLClient.mutate(any)).thenAnswer(
        (realInvocation) => getQueryResult('success_purchase_response.json'));
  }

  Future<void> setUpFailedBuyResponse() async {
    when(mockGraphQLClient.mutate(any)).thenAnswer(
        (realInvocation) => getQueryResult('failed_purchase_response.json'));
  }

  group('getCustomer', () {
    test('should get customer succesfully', () async {
      await setUpSuccessCustomerResponse();
      var response = await remoteDataSource.getCustomerModel();
      verify(mockNetworkInfoImpl.isConnected).called(1);
      verify(mockGraphQLClient.query(any)).called(1);
      expect(response.isRight(), true);
    });

    test('Unauthorized token should retunr failure', () async {
      await setUpFailedCustomerResponse();
      var response = await remoteDataSource.getCustomerModel();
      verify(mockNetworkInfoImpl.isConnected).called(1);
      verify(mockGraphQLClient.query(any)).called(1);
      expect(response.isLeft(), true);
    });
  });

  group('make purchase', () {
    test('should buy product succesfully', () async {
      await setUpSuccessBuyResponse();
      var response = await remoteDataSource.buyProduct("offerId");
      verify(mockNetworkInfoImpl.isConnected).called(1);
      verify(mockGraphQLClient.mutate(any)).called(1);
      expect(response.isRight(), true);
    });

    test('shoud fail to buy product', () async {
      await setUpFailedBuyResponse();
      var response = await remoteDataSource.buyProduct("offerId");
      verify(mockNetworkInfoImpl.isConnected).called(1);
      verify(mockGraphQLClient.mutate(any)).called(1);
      expect(response.isRight(), false);
    });
  });
}
