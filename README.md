# nubank_code_challenge

## Description
- First Screen shows customer info and a list of offers.
- You can select each offer to see its details.
- On the details screen, you can buy a product.
- The app will evaluate if the balance is enough to make a purchase, otherwise, it will return an insufficient balance warning.
- After the purchase is made, the app will update the customer's balance and store it on shared preferences (in case the user wants to make a 2nd purchase afterward).
- If you close the application and open it again, the cache will be reset to server default values.

## Arquitecture 
- BLoC Pattern.
- S.O.L.I.D. Principles.
- Clean Arquitecture.
- Dependency Injection using Get It library.

## Libraries Used
- Grapql (to connect with server).
- flutter_bloc (to implement BLoC Pattern).
- dartz (to make use of some functional programming).
- equatable (to evaluate objects).
- internet_connection_checker (to check internet connection status).
- get_it (to make dependency injection).
- json_annotation (to make serializable objects in the data layer, so I could parse them from JSON easily).
- shared_preferences (to store customer balance and update it after purchase).
- build_runner (to make serializable objects and mockito classes).
- json_serializable (to make serializable objects).
- mockito (to create mock classes using build runner).
- bloc_test (to test bloc events and states).
